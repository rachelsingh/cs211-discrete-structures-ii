 
import java.util.Scanner;
public class Ch7Proj
{	
	static Integer travelerPosition = new Integer(0);

	static Integer cabbagePosition = new Integer(0);

	static Integer goatPosition = new Integer(0);

	static Integer wolfPosition = new Integer(0);
	
	static String[] moveWho = { "", "Traveler", "Cabbage", "Goat", "Wolf" };

	static Scanner keyboard = new Scanner(System.in);
	static int userInput;
	public static void main(String[] args) 
	{
		boolean done = false;
		while (!done)
		{
			diplayState(travelerPosition, cabbagePosition, goatPosition, wolfPosition);
			int choice = displayMenu();
			
			/* old
			switch(choice)
			{
			case 1:move(travelerPosition, travelerPosition);
				break;
			case 2:move(cabbagePosition, travelerPosition);
				break;
			case 3:move(goatPosition, travelerPosition);
				break;
			case 4:move(wolfPosition, travelerPosition);
				break;
			case 5:System.out.println("Game has Ended."); System.exit(0);
			default:System.out.println("Error. Not a valid input.");
			}
			*/
		
			switch(choice)
			{
			case 1:  // flow through
			case 2:  // flow through
			case 3:  // flow through
			case 4:
			     moveCharacterByName( moveWho[ choice ] );
			     break;
			     
			case 5:
			 System.out.println("Game has Ended."); System.exit(0);
			default:
			 System.out.println("Error. Not a valid input.");
			}
				
		}
	}

	static public void diplayState(Integer t, Integer c, Integer g, Integer w)
	{
		System.out.println("___________________________");
		System.out.println("ISLAND 1: ");
		if (t.equals(0)) {System.out.println("Traveler \t");}
		if (c.equals(0)) {System.out.println("Cabbage \t");}
		if (g.equals(0)) {System.out.println("Goat \t");}
		if (w.equals(0)) {System.out.println("Wolf \t");}
		
		System.out.println("___________________________");
		System.out.println("ISLAND 2: ");
		if (t.equals(1)) {System.out.println("Traveler \t");}
		if (c.equals(1)) {System.out.println("Cabbage \t");}
		if (g.equals(1)) {System.out.println("Goat \t");}
		if (w.equals(1)) {System.out.println("Wolf \t");}
		System.out.println("___________________________");
		System.out.println("\n___  _____  _____  ___ ___  _____  _____  _____  ___");
	}
	
	public static int displayMenu()
	{
		System.out.println("\n1. Go to other island");
		System.out.println("2. Move Cabbage");
		System.out.println("3. Move Goat");
		System.out.println("4. Move Wolf");
		System.out.println("5. Exit Game");
		
		System.out.println(">>");
		int choice;
		userInput = keyboard.nextInt();
		choice = (userInput);
		return choice;	
	}
	
	public static void move(Integer who, Integer traveler) 
	{
		if(who.equals(travelerPosition))
		{
			moveCharacter(who);
		}
		else
		{
			System.out.println("Error: Can not move because traveler is on different island!");
		}
	}
	
	public static void moveCharacter(Integer who)
	{
	    /*
	     * This version doesn't work because you can't tell who is being moved based on the "who" variable;
	     * it only stores 0 or 1, for the position of whatever you're looking at is.
	     * Instead, you'll need to store some separate variable to know who you're moving.
	     * 
	     * Example: cabbagePosition starts at 0. You call move( cabbagePosition, travellerPosition ), so move( 0, 0 ).
	     *         This is a valid move, so it calls moveCharacter( cabbagePosition ), which is move( 0 ).
	     *         Then, inside this function, you're checking if "who == cabbagePosition" (0 == 0), or
	     *         "who == goatPositoin" (0 == 0), or "who == wolfPosition" (0 == 0), etc.
	     *         
	     *         This is why you need some way to keep track of who is moving.
	     */
		if(who.equals(0))
		{
			if(who == cabbagePosition)
			{
				System.out.println("BEFORE: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'cabbage' has a value of: " + cabbagePosition.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				cabbagePosition = 1;
				travelerPosition = 1;

				System.out.println("After 'CHANGE', The Ineger 'cabbage' has a value of: " + cabbagePosition.intValue());
				System.out.println("AFter 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				System.out.println("AFter: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("Cabbage and Traveler 'Moved' to island 2.");	}
			else if(who == goatPosition)
			{
				System.out.println("BEFORE: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'goat' has a value of: " + goatPosition.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				goatPosition = 1;
				travelerPosition = 1;

				System.out.println("After 'CHANGE', The Ineger 'goat' has a value of: " + goatPosition.intValue());
				System.out.println("AFter 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				System.out.println("AFter: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("Goat and Traveler 'Moved' to island 2.");
			}
			else if(who == wolfPosition)
			{
				System.out.println("BEFORE: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'wolf' has a value of: " + wolfPosition.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				wolfPosition = 1;
				travelerPosition = 1;

				System.out.println("After 'CHANGE', The Ineger 'wolf' has a value of: " + wolfPosition.intValue());
				System.out.println("AFter 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				System.out.println("AFter: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("Wolf and Traveler 'Moved' to island 2.");
			}
			else if(who == travelerPosition)
			{
				System.out.println("BEFORE 'CHANGE', The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				travelerPosition = 1;
				System.out.println("After 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				System.out.println("AFTER 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				System.out.println("Traveler 'Moved' to island 2.");
			}
			else
				System.out.println("Error.");
		}
		else if (who.equals(1))
		{
			if(who == cabbagePosition)
			{
				System.out.println("BEFORE: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'cabbage' has a value of: " + cabbagePosition.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				cabbagePosition = 0;
				travelerPosition = 0;

				System.out.println("After 'CHANGE', The Ineger 'cabbage' has a value of: " + cabbagePosition.intValue());
				System.out.println("AFter 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				System.out.println("AFter: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("Wolf and Traveler 'Moved' to island 1.");
			}
			else if(who == goatPosition)
			{
				System.out.println("BEFORE: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'goat' has a value of: " + goatPosition.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				goatPosition = 0;
				travelerPosition = 0;

				System.out.println("After 'CHANGE', The Ineger 'goat' has a value of: " + goatPosition.intValue());
				System.out.println("AFter 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				System.out.println("AFter: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("Wolf and Traveler 'Moved' to island 1.");
			}
			else if(who == wolfPosition)
			{
				System.out.println("BEFORE: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'wolf' has a value of: " + wolfPosition.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				wolfPosition = 0;
				travelerPosition = 0;

				System.out.println("After 'CHANGE', The Ineger 'wolf' has a value of: " + wolfPosition.intValue());
				System.out.println("AFter 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				System.out.println("AFter: The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("Wolf and Traveler 'Moved' to island 1.");
			}
			else if(who == travelerPosition)
			{
				System.out.println("BEFORE 'CHANGE', The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("BEFORE 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				travelerPosition = 0;
				System.out.println("AFTER 'CHANGE', The Ineger 'who' has a value of: " + who.intValue());
				System.out.println("AFTER 'CHANGE', The Ineger 'traveler' has a value of: " + travelerPosition.intValue());
				System.out.println("Traveler 'Moved' to island 1.");
			}
			else
				System.out.println("Error.");
		}
	}
	
	static void moveCharacterByName(String name)
    {        
        System.out.println( "" );
        System.out.println( "MOVE: " + name );
        System.out.println( "C: " + cabbagePosition + ", G: " + goatPosition + ", W: " + wolfPosition + ", T: " + travelerPosition );
        System.out.println( "" );
        
        if ( name.equals( "Cabbage" ) && cabbagePosition.equals( travelerPosition ) )
        {
            if ( cabbagePosition == 0 )
            {
                cabbagePosition = 1;
                travelerPosition = 1;
            }
            else
            {
                cabbagePosition = 0;
                travelerPosition = 0;
            }
        }
        else if ( name.equals( "Goat" ) && goatPosition.equals( travelerPosition ) )
        {
            if ( goatPosition == 0 )
            {
                goatPosition = 1;
                travelerPosition = 1;
            }
            else
            {
                goatPosition = 0;
                travelerPosition = 0;
            }
        }
        else if ( name.equals( "Wolf" ) && wolfPosition.equals( travelerPosition ) )
        {
            if ( wolfPosition == 0 )
            {
                wolfPosition = 1;
                travelerPosition = 1;
            }
            else
            {
                wolfPosition = 0;
                travelerPosition = 0;
            }
        }
        else if ( name.equals( "Traveler" ) )
        {
            if ( travelerPosition == 0 )
            {
                travelerPosition = 1;
            }
            else
            {
                travelerPosition = 0;
            }
        }
        else
        {
            System.out.println( "Invalid move!" );
        }
    }
	
	boolean GameOver(int t, int c, int g, int w)
	{
		return false;
	}
	
	boolean gameWon(int t, int c, int g, int w)
	{
		if (t == 1 && c == 1 && g == 1 && w == 1)
		{
		return true;
		}
		else
			return false;
	}
	
}
