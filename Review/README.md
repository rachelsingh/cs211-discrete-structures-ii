# Disclaimer

Sometimes when I'm grading exams I notice errors I put in the keys.
Sometimes, I remember to go back and fix the keys, sometimes it slips
my mind with the busyness of the semester.

Let me know in class if you find an error and I will fix it on the key.
